import { Context, SessionFlavor, session } from "grammy";

type RecordValue = {
    timeout: NodeJS.Timeout,
}

type Session = {
    records: Record<number, RecordValue>,
};

export type CustomContext = Context & SessionFlavor<Session>;

export const customsession = session({ initial: (): Session => ({
    records: {},
})})
