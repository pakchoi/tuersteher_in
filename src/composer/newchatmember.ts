import { Composer, InlineKeyboard } from "grammy";
import { kick } from "../helpers";
import { CustomContext } from "../session";

const TIMEOUT_IN_MIN = 5;
const TIMEOUT_IN_SEC = TIMEOUT_IN_MIN * 60;

export const newchatmembers = new Composer<CustomContext>();

newchatmembers.on(':new_chat_members', async ctx => {
    ctx.message?.new_chat_members.map(async new_chat_member => {
        const restricted = await ctx.restrictChatMember(new_chat_member.id, { can_send_messages: false });
        console.log(`${restricted ? '[INFO] Restricted' : '[WARN] Could not restrict'} ${new_chat_member.id}`);

        const g = (a: boolean) => JSON.stringify({ userId: new_chat_member.id, correct: a });

        const keyboard = new InlineKeyboard()
            .text("Literatur", g(false))
            .text("Fahrscheinkontrollen", g(true)).row()
            .text("Vegane Rezepte", g(false))
            .text("Kunstgeschichte", g(false)).row();

        const challengeMsg = await ctx.reply(`
*Willkommen ${new_chat_member.first_name}!* Da in letzter Zeit viele Spam-Bots \
die Gruppe heimsuchen, müssen wir verifizeren, dass du keiner bist. Bitte \
beantworte dafür die untenstehende Frage. Bis du sie korrekt beantwortest, \
kannst du noch keine Nachrichten in die Gruppe schicken. Beantwortest du sie \
innerhalb ${TIMEOUT_IN_MIN} Minuten nicht, wirst du wieder aus der Gruppe \
entfernt (du kannst aber wieder eintreten und es nochmal versuchen). \
Entschuldige die Umstände. Die Frage lautet:

Worum geht es in dieser Gruppe hauptsächlich?
`, {
            reply_to_message_id: ctx.message?.message_id,
            parse_mode: "Markdown",
            disable_notification: true,
            reply_markup: keyboard,
        });

        const timeout = setTimeout(async () => {
            ctx.api.editMessageText(
                challengeMsg.chat.id,
                challengeMsg.message_id,
                `
${challengeMsg.text}

*Update:*
Sorry, leider wurde keine Eingabe vernommen und du wirst aus der Gruppe \
entfernt. Sollte es sich hierbei um ein Irrtum handeln, so kannst du wieder \
beitreten und es erneut versuchen.
                `,

                {
                    parse_mode: "Markdown",
                }
            );
            const kicked = await kick(ctx, new_chat_member);
            console.log(`${kicked ? '[INFO] Kicked' : '[WARN] Failed to kick'} ${new_chat_member.id}`)
            delete ctx.session.records[new_chat_member.id];
        }, TIMEOUT_IN_SEC * 1_000);

        ctx.session.records[new_chat_member.id] = { timeout };
    });
});
