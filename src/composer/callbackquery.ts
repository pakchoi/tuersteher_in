import { Composer } from "grammy";
import { kick } from "../helpers";
import { CustomContext } from "../session";

const TIME_TO_DELETE_IN_SEC = 60;

type CallbackQueryData = {
    userId: number,
    correct: boolean,
}

export const callbackquery = new Composer<CustomContext>();

callbackquery.on('callback_query', async ctx => {
    const userWhoClicked = ctx.callbackQuery.from;

    try {
        if (!ctx.callbackQuery.data) return;
        const data = JSON.parse(ctx.callbackQuery.data) as CallbackQueryData;
        if (userWhoClicked.id !== data.userId) return; // somebody else clicked
        const record = ctx.session.records[userWhoClicked.id];
        if (!record) return; // weird, we should have a record on them
        clearTimeout(record.timeout);

        if (data.correct) {
            const unrestricted = await ctx.restrictChatMember(userWhoClicked.id, { can_send_messages: true });
            console.log(`${unrestricted ? '[INFO] Unrestricted' : '[WARN] Could not unrestrict'} ${userWhoClicked.id}`);
            ctx.editMessageText(`
${ctx.callbackQuery.message?.text}

*Update:* Danke, alles geklärt! (Diese Nachricht wird in
${TIME_TO_DELETE_IN_SEC} Sekunden gelöscht.)
            `, {
                parse_mode: "Markdown",
            });
        }
        else {
            ctx.editMessageText(`
${ctx.callbackQuery.message?.text}

*Update:* Sorry, deine Eingabe war nicht richtig und du wirst aus der Gruppe \
entfernt. Sollte es sich hierbei um ein Irrtum handeln, so kannst du wieder \
beitreten und es erneut versuchen.
            `, {
                parse_mode: "Markdown",
            });
            kick(ctx, userWhoClicked);
        }
        delete ctx.session.records[userWhoClicked.id];
        setTimeout(() => ctx.deleteMessage(), TIME_TO_DELETE_IN_SEC * 1_000);
    }
    catch (e) {
        console.error(`Error trying to parse callbackQuery.data: ${e}`);
    }
});
