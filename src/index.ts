import { Bot } from "grammy";
import { exit } from "process";
import { callbackquery } from "./composer/callbackquery";
import { newchatmembers } from "./composer/newchatmember";
import { CustomContext, customsession } from "./session";

console.log("Starting up tuersteher_in...");
if (!process.env.BOT_TOKEN) {
    console.error("BOT_TOKEN environment variable not set, exiting...");
    exit(0);
}

const bot = new Bot<CustomContext>(process.env.BOT_TOKEN);

bot.use(customsession);
bot.use(newchatmembers);
bot.use(callbackquery);
bot.start();

console.log("Bot started.");
