import { Context } from "grammy";
import { User } from "@grammyjs/types";

export const kick = async (ctx: Context, user: User) => {
    if (await ctx.banChatMember(user.id)) {
        return await ctx.unbanChatMember(user.id);
    }
    return false;
};
